//Function drag and drop
function allowDrop(ev) {
  ev.preventDefault();
}

function drag(ev) {
  ev.dataTransfer.setData("text", ev.target.id);
  document.getElementById("colorPicker").style.background = "rgb(173, 217, 255)";
}

//Fonction swap 2 class whene father objet is drop on
function drop(ev, target) {
  ev.preventDefault();
  var data = ev.dataTransfer.getData("text");
  document.getElementById(target.id).classList.remove(document.getElementById(target.id).className.split(' ')[1]);
  document.getElementById(target.id).classList.add(document.getElementById(data).className.split(' ')[1]);
}
document.addEventListener("dragend", function( event ) {
    document.getElementById("colorPicker").style.background = "#f0f0f0";
}, false);




//Erase color of the selector when click on
function eraseColor(){
  for (var i = 1; i < 5; i++) {
    document.getElementById(i).classList.remove(document.getElementById(i).className.split(' ')[1]);
    document.getElementById(i).classList.add("cDefaut");
  }
}

//Return value beetween min and max
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function returnColor(){
  var returnColor;
  switch (getRandomInt(1, 6)) {
    case 1:
      returnColor = "color1";
    break;
    case 2:
      returnColor = "color2";
    break;
    case 3:
      returnColor = "color3";
    break;
    case 4:
      returnColor = "color4";
    break;
    case 5:
      returnColor = "color5";
    break;
    case 6:
      returnColor = "color6";
    break;
    default:
      console.log('Error');
  }
  return returnColor;
}

function scanColor(){
  var col1 = document.getElementById(1).className.split(' ')[1];
  var col2 = document.getElementById(2).className.split(' ')[1];
  var col3 = document.getElementById(3).className.split(' ')[1];
  var col4 = document.getElementById(4).className.split(' ')[1];

  return [col1, col2, col3, col4];
}

//return number of good color, and color at the good emplacement
function checkPlacement(tabCombination){
  var goodColor = 0;
  var colorPicker = scanColor();
  var goodPlacement = 0;
  for (var i = 0; i < 4; i++) {
    if (tabCombination[i] == colorPicker[i]) {
      goodPlacement++;
      colorPicker[i] = 0;
    }
  }
  //test good color
  for (var i = 0; i < 4; i++) {
    for (var a = 0; a < 4; a++) {
      if (tabCombination[i] == colorPicker[a]) {
        goodColor++;
        colorPicker[a] = 0;//If color equal to 0 it can be equal to another color
      }
    }
  }
  return [goodPlacement, goodColor];
}

function winGame(tabCombination){
  return checkPlacement(tabCombination)[0] == 4;
}

// function checkPutColor(nBT, gP, gC){
//   if (gP + gC > 4) {
//     console.log("Erreur");
//     return;
//   }
//   var checkID;
//   var rang = 1;
//   for (var i = 1; i < gP+1; i++) {
//     console.log("couleir rouge : "+ i);
//     checkID = "Cc" + String(nBT) + String(i);
//     // console.log("id rouge" + checkID);
//     document.getElementById(checkID).classList.remove("cDefaut");
//     document.getElementById(checkID).classList.add("color3");
//     //console.log("nombre gP : " + gP);
//     rang = i;
//   }
//   // console.log("de" + (rang+1) +" a "+ (gC+1+rang));
//   for (var a = rang + 1; a < gC+1+rang; a++) {
//     checkID = "Cc" + String(nBT) + String(a);
//     console.log("couleir bleu : "+ a);
//     document.getElementById(checkID).classList.remove("cDefaut");
//     document.getElementById(checkID).classList.add("color2");
//   }
// }

function checkPutColor(nBT, gP, gC){
  var checkID;
  var gPC = gP;
  var gCC = gC;
  if (gP + gC > 4) {
    console.log("Erreur");
    return;
  }
  for (var i = 1; i < gP + gC + 1; i++) {
    checkID = "Cc" + String(nBT) + String(i);
    if (gPC!=0) {
      document.getElementById(checkID).classList.remove("cDefaut");
      document.getElementById(checkID).classList.add("color2");
      gPC--;
    }else if(gCC!=0){
      document.getElementById(checkID).classList.remove("cDefaut");
      document.getElementById(checkID).classList.add("color6");
      gCC--;
    }else {
      console.log("Erreur");
    }
  }
}

function RedirectionPerdu(){
  document.location.href="perdu.html";
}

function RedirectionGagne(){
  document.location.href="gagne.html";
}


function RedirectionMain(){
  document.location.href="main.html";
}

function testDuBouton(){
  var winGame2 = false;
  var idPlacement;
  var testVoid = 0;
  console.log("entree status : "+winGame2);


  //document.getElementById("apresTest").style.display = "block";


  for (var i = 0; i < 4; i++) {//test if miss one color on color picker
    if(scanColor()[i] == "cDefaut"){
      testVoid++;
    }
  }
  if (testVoid == 0 && nbTour<=9 && winGame2 != true && nbTour<10) {
    console.log("Toutes les cases sont remplies");
    checkPutColor(nbTour, checkPlacement(createColorCombination)[0], checkPlacement(createColorCombination)[1]);
    winGame2 = winGame(createColorCombination);
    for (var i = 1; i < 5; i++) {
      idPlacement = nbTour + String(i);
      document.getElementById(idPlacement).classList.remove("cDefaut");
      document.getElementById(idPlacement).classList.add(scanColor()[i-1]);
    }
    eraseColor();
    document.getElementById("apresTest").style.display = "block";
      console.log("frame remplissage status : "+winGame2);
    nbTour++;
  }else {
    window.alert("Il y a une case vide");
  }

  if (winGame(createColorCombination) == true || winGame2 == true) {
    RedirectionGagne();
  }

  if (nbTour>9) {
    RedirectionPerdu();
  }
  console.log("sortie de frame" + winGame(createColorCombination));
}

//Main programm
var createColorCombination = [returnColor(),returnColor(),returnColor(),returnColor()];
var nbTour = String(1);

for(var i = 0; i<4; i++){
  console.log(createColorCombination[i]);
}

function help(ev){
  //window.alert(document.getElementById(ev.id).id);
  var idSwitch = document.getElementById(ev.id).id;
  document.getElementById("hHover").style.display = "block";

  switch (idSwitch) {
    case "palette":
      document.getElementById('helpHover').innerHTML = "Palette";
    break;
    case "colorPicker":
      document.getElementById('helpHover').innerHTML = "Bas du plateau";
    break;
    case "boardGame":
      document.getElementById('helpHover').innerHTML = "Plateau";
    break;
  default:
      console.log("erreur switch");
  }
}

function nohelp(ev){
  document.getElementById("hHover").style.display = "none";
}

function funcCheckBox(){
  var checkBox = document.getElementById("scales");
  var x = document.getElementById("helpContainer");
   // Get the output text

   // If the checkbox is checked, display the output text
   if (checkBox.checked == true){
     x.style.display = "block";
   } else {
     x.style.display = "none";
   }
}

function main(){
   document.getElementById("scales").checked = true;
}
